package kz.aitu.week4.stack;

public class Main {
    public static int print(Stack stack){
        Node block = stack.getHead();
        int y = 0;
        while (block!=null){
            System.out.print(block.getValue()+" ");
            block = block.getNext();
            y++;
        }
        System.out.print("Size: ");
        return y;
    }

    public static void main(String[] args) {
        Stack newStack = new Stack();
        newStack.push(1);
        newStack.push(2);
        newStack.pop();
        newStack.empty();
        System.out.println("Size: "+newStack.size(newStack));
        newStack.getTop();
    }
}
