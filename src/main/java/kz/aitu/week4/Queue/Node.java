package kz.aitu.week4.queue;

public class Node {
    private int value;
    private Node next;

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node(int value){
        this.value = value;
    }
}
