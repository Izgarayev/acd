package kz.aitu.endterm;

public class BSTree {
    private int CountOdd(Node root){
        if (root == null){
            return 0;

        boolean val = (root.value%2 == 1);

        return val + CountOdd(root.left) + CountOdd(root.right);
    }
}
