package kz.aitu.week4.stack;

public class Stack {
    private Node head;

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getHead() {
        return head;
    }

    public void push(int value){
        Node newNode = new Node(value);
        if (head == null){
            head = newNode;
        } else {
            newNode.setNext(head);
            head = newNode;
        }
    }

    public void pop(){
        Node currentB = head;
        head = currentB.getNext();
    }

    public void empty(){
        if (head==null){
            System.out.println("Stack is empty");
        } else {
            System.out.println("Stack is not empty");
        }
    }

    public int size(Stack stack){
        Node top = stack.getHead();
        int u = 0;
        while (top!=null){
            top =top.getNext();
            u++;
        }
        return u;
    }

    public void getTop(){
        System.out.println(head.getValue());
    }
}
